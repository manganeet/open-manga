# Open Manga
This is just an Open Manga Project...

## How to Use
You are able to read all open manga here with using [Raw GitHack](https://raw.githack.com) and [ImgFo](https://imgfo.com) Application.

## Contribution
Any contributions are welcome...

## Code of Conduct
- Json Structure must follow [ImgFo](https://imgfo.com).
- Json data must not contains abuse, sensitive, dirty or bad words.
- Image should covered with any CDN.

## Backers
- Yahzee
- Umild7
- XCleopatra